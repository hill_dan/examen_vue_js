import Accueil from "../components/pages/Accueil"
import Login from "../components/pages/Login"
import Compte from "../components/pages/Compte"
import ShowArticle from "../components/articles/ShowArticle"
// import {store} from "@/store/store"  -> pour l'utilisation dans beforeEnter
export const routes = [
    {path: "/", name: "accueil", component: Accueil},
    {path: "/login", name: "login", component: Login},
    {
        path:"/compte", name:"compte",component:Compte,
        beforeEnter: (to, from, next) => {
            /* Exemple d'utilisation beforeEnter routes.js
            console.log(store.state);
            if (store.state....) {
                next({name:"home",params:{}})
            } else {
                // next("/about")
                //next({path:"/about"})
                next({name: "about"})
            }*/
            // next({path:"/"});
            next();
        },
    },
    {path:"/article/:id", name:"showId",component:ShowArticle},
    {path:"/*", redirect:"/"},

    // Voir exemple de 'beforeEach hook' dans router.js

    // Voir exemple de 'beforeRouteEnter beforeRouteUpdate beforeRouteLeave' dans le composant Demo.vue
]