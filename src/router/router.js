import Vue from "vue"
import Router from "vue-router"

import {routes} from "./routes"

Vue.use(Router);

// create object router with the valid initialization
export const router = new Router({
    mode: "history",
    routes
})

// beforeEach hook - Exécuter avant TOUTES les routes
router.beforeEach((to, from, next) => {
    console.log("------beforeEach------Avant toutes les routes----");
    console.log(to);
    console.log("------fin beforeEach------");
    next();
})