import Vue from "vue"
import VueX from "vuex"
import axios from "axios"

Vue.use(VueX)

export const store = new VueX.Store({
    state: {
        isConnected: false,
        articles: null,
        detailsArticleEnCours: {},
        laCommande: [],
        totalCommande: 0,
        affInfosPanier: false,
        affInfosCredit: false,
        solde: 150,
        soldeSuff: true,
        siPaye: false,
        utilisateur: "",
        ifMessage: false,
        leMessage: "",
    },
    getters: {
        getNbrCommande(state) {
            return state.laCommande.length;
        },
    },
    mutations: {
        chargementDesArticles(state, data) {
            state.articles = data;
        },
        deconnexion(state) {
            state.isConnected = false;

            // autre solution pour info:
            // localStorage.isLogged = false;
        },
        connexion(state, user) {
            state.isConnected = true;
            state.utilisateur = user;

            // autre solution pour info:
            // localStorage.isLogged = true;
        },
        clickAffPanier(state) {
            if (state.affInfosPanier) {
                state.affInfosPanier = false;
            } else {
                state.affInfosPanier = true;
                state.affInfosCredit = false;
            }
        },
        clickAffCredit(state) {
            if (state.affInfosCredit) {
                state.affInfosCredit = false;
            } else {
                state.affInfosCredit = true;
                state.affInfosPanier = false;
                // Temporisation solution 1 (dans mutations) 
                setTimeout(() => {
                    state.affInfosCredit = false;
                }, 7500)
            }
        },

        // récupère les détails de l'article visé par l'id et les places dans 
        // state.detailsArticleEnCours pour afficher ces données dans ShowArticle.vue
        // voir première partie dans 'actions'
        getArticleById(state, id) {
            let detailsArticle = state.articles.filter(e => e.id === id);
            state.detailsArticleEnCours = detailsArticle[0];
        },
        addCommande(state, data) {
            // Recherche si il y a déjà une commande pour cette article
            let comExist = state.laCommande.filter(e => e.idArticle === data.id);
            // Retrouve les détails de l'article visé par l'id
            let tmp = state.articles.filter(e => e.id === data.id);
            // ou const idCommande = state.laCommande.find(element => element.idArticle == data.id);

            if (comExist.length === 0) {
                // Nouvelle commande pour cet article !

                let tmpNvCommande = {};
                tmpNvCommande.id = Date.now() + Math.random();
                tmpNvCommande.idArticle = data.id;
                tmpNvCommande.article = tmp[0].name;
                tmpNvCommande.image = tmp[0].image;
                tmpNvCommande.quantite = data.nombre;
                tmpNvCommande.prix = tmp[0].price;
                tmpNvCommande.prixTot = null;
                state.laCommande.unshift({ ...tmpNvCommande });
            } else {
                // Une commande existe déjà pour cet article !
                const artCommande = state.laCommande.find(element => element.idArticle == data.id);
                artCommande.quantite += data.nombre;
            }
            // Si Ajouter et non Retirer
            if (data.nombre > 0) {
                // Affiche message 'Ajouté au panier'
                state.leMessage = `${data.nombre} X ${tmp[0].name} - Ajouté au panier`;
                state.ifMessage = true;
                setTimeout(() => {
                    state.ifMessage = false;
                }, 3000)
            }
        },
        suppCommande(state, idArt) {
            // Suppression dans la commande 
            state.laCommande.splice(state.laCommande.findIndex(el => el.idArticle === idArt), 1);
        },
        calculDesSommes(state) {
            let sum = 0;
            state.laCommande.forEach(comm => {
                comm.prixTot = Math.round(comm.prix * comm.quantite * 100) / 100;
                sum += comm.prix * comm.quantite;
            });
            state.totalCommande = Math.round(sum * 100) / 100;
            // OU var ton_chiffre = 12.3455633; // utilisation de la class Number
            // ton_chiffre.toFixed(2); //r'enveras 12.35..

            // Vérifie si le solde est suffisant
            state.soldeSuff = state.totalCommande > state.solde ? false : true;
        },
        ajCredit(state) {
            state.solde += 200;

            // Vérifie si le solde est suffisant
            state.soldeSuff = state.totalCommande > state.solde ? false : true;
        },
        payer(state) {
            state.solde = Math.round((state.solde - state.totalCommande) * 100) / 100;
            state.laCommande = [];
            state.siPaye = true;
            setTimeout(() => {
                state.siPaye = false;
            }, 4500)
        },
    },
    actions: {
        chargementDesArticles({ commit }) {
            axios.get("https://5df6a0ebc1b62e0014e2088c.mockapi.io/products").then(res => {
                commit("chargementDesArticles", res.data)
            })
        },
        deconnexion({ commit }) {
            commit("deconnexion")
        },
        connexion({ commit }, user) {
            commit("connexion", user)
        },
        clickAffPanier({ commit, state }) {
            // Temporisation solution 2 (dans Actions) (c'est Mieux ?) 
            setTimeout(() => {
                state.affInfosPanier = false;
            }, 9500);
            commit("clickAffPanier")
        },
        clickAffCredit({ commit }) {
            commit("clickAffCredit")
        },
        getArticleById({ commit }, id) {
            commit("getArticleById", id)
        },
        addCommande({ commit }, data) {
            commit("addCommande", data)
            commit("calculDesSommes")
        },
        suppCommande({ commit }, idArt) {
            commit("suppCommande", idArt)
            commit("calculDesSommes")
        },
        calculDesSommes({ commit }) {
            commit("calculDesSommes")
        },
        ajCredit({ commit }) {
            commit("ajCredit")
        },
        payer({ commit }) {
            commit("payer")
        },
    },
})