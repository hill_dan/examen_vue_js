export const dataMixin = {
    filters:{
        formatDate(el) {
            let arrayDate = el.split('T');
            let arrayDateDate = arrayDate[0].split('-') ;
            let arrayDateHeure = arrayDate[1].split(':') ;
            let mois = "";
            switch (arrayDateDate[1]) {
                case '1':
                  mois = 'Janvier';
                  break;
                case '2':
                  mois = 'Février';
                  break;
                case '3':
                  mois = 'Mars';
                  break;
                case '4':
                  mois = 'Avril';
                  break;
                case '5':
                  mois = 'Mai';
                  break;
                case '6':
                  mois = 'Juin';
                  break;
                case '7':
                  mois = 'Juillet';
                  break;
                case '8':
                  mois = 'Août';
                  break;
                case '9':
                  mois = 'Septembre';
                  break;
                case '10':
                  mois = 'Octobre';
                  break;
                case '11':
                  mois = 'Novembre';
                  break;
                case '12':
                  mois = 'Décembre';
                  break;
                default:
                  mois = '-Error-';
            }
            return el = arrayDateDate[2] + " " + mois + " " + arrayDateDate[0] + " - " + arrayDateHeure[0] + "h " + arrayDateHeure[1] + "m "
        },
        mySplit(el, arg1, arg2) {
            let arraySplit = el.split(arg1);
            return arraySplit[arg2-1]
      }
    }
}