import Vue from 'vue'
import App from './App.vue'
import {router} from "./router/router"
import {store} from "./store/store"

Vue.config.productionTip = false

// Ma directive GLOBALE ( Ma directive Locale:  voir MyInputSelect.vue )
Vue.directive("componentUpdate",{
  bind(){},
  componentUpdated(el){
    let couleurOrg = el.style.color;
    el.style.color = "yellow";
    setTimeout(() => {
      el.style.color = "green";
      setTimeout(() => {
        el.style.color = couleurOrg;
      }, 120)
    }, 120)

  }
})
// Utilisation:
// <span v-componentUpdate> specialInput:{{ specialInput }} </span>


new Vue({
  router,store,
  render: h => h(App),
}).$mount('#app')
